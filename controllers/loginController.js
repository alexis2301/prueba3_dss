const pool = require('../database/config')
const utils = require('../resources/utils')
const jwt = require('jsonwebtoken')

const loginForm = (request,response) =>{
    response.render('login')
}

const doLogin = async (request,response) => {

    const res = await pool.query(
        `select * from users where username = ?
         and password = ?`,
         [
            request.body.username,
            request.body.password
         ]
        );
    if(res.id){
        respuesta = {
            message:"Login Exitoso",
            userData : results,
            token : process.env.TOKEN
        }
        response.render('app/dashboard',{locals:respuesta})
    }else{
        respuesta = {
            message:"Login Fallido",
            userData : '',
            token : ''
        }
        response.render('app/error',{locals:respuesta})
    }

}

module.exports = {
    loginForm,
    doLogin,
}